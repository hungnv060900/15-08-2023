import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class Task52B40 {
    // Yêu cầu 1
    public void sortArrayList(ArrayList<Integer> list) {
        Collections.sort(list);
    }

    // Yêu cầu 2
    public ArrayList<Integer> createNewArrayList() {
        ArrayList<Integer> newList = new ArrayList<>();
        for (int i = 10; i <= 100; i += 10) {
            newList.add(i);
        }
        return newList;
    }

    // Yêu cầu 3
    public void checkYellow(ArrayList<String> list) {
        if (list.contains("yellow")) {
            System.out.println("OK");
        } else {
            System.out.println("KO");
        }
    }

    // Yêu cầu 4
    public int sumArrayList(ArrayList<Integer> list) {
        int sum = 0;
        for (int num : list) {
            sum += num;
        }
        return sum;
    }

    // Yêu cầu 5
    public void clearArrayList(ArrayList<String> list) {
        list.clear();
        System.out.println(list);
    }

    // Yêu cầu 6
    public void shuffleArrayList(ArrayList<String> list) {
        Collections.shuffle(list);
        System.out.println(list);
    }

    // Yêu cầu 7
    public void reverseArrayList(ArrayList<String> list) {
        Collections.reverse(list);
        System.out.println(list);
    }

    // Yêu cầu 8
    public ArrayList<String> subListArrayList(ArrayList<String> list) {
        return new ArrayList<>(list.subList(2, 8));
    }

    // Yêu cầu 9
    public void swapElements(ArrayList<String> list, int index1, int index2) {
        Collections.swap(list, index1, index2);
        System.out.println(list);
    }

    // Yêu cầu 10
    public void copyArrayLists(ArrayList<Integer> source, ArrayList<Integer> destination) {
        // Ensure destination has enough elements before copying
        while (destination.size() < source.size()) {
            destination.add(0);
        }
        Collections.copy(destination, source);
        System.out.println("Source ArrayList: " + source);
        System.out.println("Destination ArrayList: " + destination);
    }

    public static void main(String[] args) {
        Task52B40 task = new Task52B40();
        ArrayList<Integer> list1 = new ArrayList<>();
        ArrayList<Integer> list2 = new ArrayList<>();

        // Thêm số vào ArrayLists
        for (int i = 1; i <= 10; i++) {
            list1.add(i);
            list2.add(i * 2);
        }

        // Gọi các hàm thực hiện yêu cầu tương ứng và in ra kết quả
        task.sortArrayList(list1);
        System.out.println(list1);

        ArrayList<Integer> newList = task.createNewArrayList();
        System.out.println(newList);

        ArrayList<String> colorList = new ArrayList<>(Arrays.asList("red", "blue", "green", "yellow", "orange"));
        task.checkYellow(colorList);

        int sum = task.sumArrayList(list2);
        System.out.println("Sum of ArrayList: " + sum);

        task.clearArrayList(colorList);

        task.shuffleArrayList(colorList);

        task.reverseArrayList(colorList);

        ArrayList<String> subList = task.subListArrayList(colorList);
        System.out.println(subList);

        task.swapElements(colorList, 2, 6);

        task.copyArrayLists(list1, list2);
    }
}
