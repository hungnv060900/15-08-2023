public class Task52B10 {
    public void findDuplicateCharacters(String input) {
        input = input.toLowerCase();
        boolean hasDuplicates = false;
        StringBuilder duplicates = new StringBuilder();

        for (int i = 0; i < input.length(); i++) {
            char c = input.charAt(i);
            if (input.indexOf(c) != input.lastIndexOf(c)) {
                if (duplicates.indexOf(String.valueOf(c)) == -1) {
                    duplicates.append(c).append(" ");
                    hasDuplicates = true;
                }
            }
        }

        if (hasDuplicates) {
            System.out.println(duplicates);
        } else {
            System.out.println("NO");
        }
    }

    public String checkReverse(String str1, String str2) {
        String reverseStr2 = new StringBuilder(str2).reverse().toString();
        if (str1.equalsIgnoreCase(reverseStr2)) {
            return "OK";
        } else {
            return "KO";
        }
    }

    public void findUniqueCharacter(String input) {
        input = input.toLowerCase();
        for (int i = 0; i < input.length(); i++) {
            char c = input.charAt(i);
            if (input.indexOf(c) == input.lastIndexOf(c)) {
                System.out.println(c);
                return;
            }
        }
        System.out.println("NO");
    }

    public String reverseString(String input) {
        return new StringBuilder(input).reverse().toString();
    }

    public boolean containsDigits(String input) {
        for (char c : input.toCharArray()) {
            if (Character.isDigit(c)) {
                return true;
            }
        }
        return false;
    }

    public int countVowels(String input) {
        input = input.toLowerCase();
        int count = 0;
        for (char c : input.toCharArray()) {
            if ("aeiou".contains(String.valueOf(c))) {
                count++;
            }
        }
        return count;
    }

    public int convertToInt(String input) {
        return Integer.parseInt(input);
    }

    public String replaceCharacters(String str, String oldChars, String newChars) {
        for (int i = 0; i < oldChars.length(); i++) {
            str = str.replace(oldChars.charAt(i), newChars.charAt(i));
        }
        return str;
    }

    public String reverseWords(String input) {
        String[] words = input.split(" ");
        StringBuilder reversed = new StringBuilder();
        for (int i = words.length - 1; i >= 0; i--) {
            reversed.append(words[i]);
            if (i > 0) {
                reversed.append(" ");
            }
        }
        return reversed.toString();
    }

    public boolean isPalindromeAfterReverse(String input) {
        String reversed = new StringBuilder(input).reverse().toString();
        return input.equalsIgnoreCase(reversed);
    }

    public static void main(String[] args) throws Exception {
        // System.out.println("Hello, World!");
        Task52B10 task = new Task52B10();

        task.findDuplicateCharacters("Java"); // Output: a
        task.findDuplicateCharacters("Devcamp JAVA exercise"); // Output: a e
        task.findDuplicateCharacters("Devcamp"); // Output: NO
        task.findDuplicateCharacters("a"); // Output: NO
        task.findDuplicateCharacters("eca"); // Output: e a
        task.findDuplicateCharacters("NO"); // Output: NO

        System.out.println(task.checkReverse("word", "drow")); // Output: OK
        System.out.println(task.checkReverse("java", "js")); // Output: KO

        task.findUniqueCharacter("Java"); // Output: j
        task.findUniqueCharacter("Haha"); // Output: NO
        task.findUniqueCharacter("Devcamp"); // Output: e

        System.out.println(task.reverseString("word")); // Output: drow

        System.out.println(task.containsDigits("abc")); // Output: false
        System.out.println(task.containsDigits("a1bc")); // Output: true

        System.out.println(task.countVowels("java")); // Output: 2
        System.out.println(task.countVowels("devcamp")); // Output: 2

        System.out.println(task.convertToInt("1234")); // Output: 1234

        System.out.println(task.replaceCharacters("devcamp java", "ab", "bf")); // Output: devcbmp jbvb
        System.out.println(task.replaceCharacters("exercise", "e", "f")); // Output: fxfrcisf

        System.out.println(task.reverseWords("I am developer")); // Output: developer am I

        System.out.println(task.isPalindromeAfterReverse("aba")); // Output: true
        System.out.println(task.isPalindromeAfterReverse("abc"));
    }
}
