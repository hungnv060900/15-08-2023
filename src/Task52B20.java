import java.util.ArrayList;
import java.util.Iterator;

public class Task52B20 {
    // task1
    public ArrayList<String> task1() {
        ArrayList<String> color = new ArrayList<>();
        color.add("red");
        color.add("blue");
        color.add("greem");
        color.add("yellow");
        color.add("gray");
        return color;
    }

    // task2
    // Yêu cầu 2
    public ArrayList<Integer> sumArrayLists(ArrayList<Integer> list1, ArrayList<Integer> list2) {
        ArrayList<Integer> sumList = new ArrayList<>();
        for (int i = 0; i < Math.min(list1.size(), list2.size()); i++) {
            sumList.add(list1.get(i) + list2.get(i));
        }
        return sumList;
    }

    // Yêu cầu 3
    public ArrayList<String> createColorArrayList() {
        ArrayList<String> colorList = new ArrayList<>();
        colorList.add("Red");
        colorList.add("Green");
        colorList.add("Blue");
        colorList.add("Yellow");
        colorList.add("Orange");
        return colorList;
    }

    // Yêu cầu 4 và 5
    public String getElementAtIndex(ArrayList<String> list, int index) {
        if (index >= 0 && index < list.size()) {
            return list.get(index);
        }
        return "";
    }

    public String getLastElement(ArrayList<String> list) {
        if (!list.isEmpty()) {
            return list.get(list.size() - 1);
        }
        return "";
    }

    // Yêu cầu 6
    public ArrayList<String> removeLastElement(ArrayList<String> list) {
        ArrayList<String> updatedList = new ArrayList<>(list);
        if (!updatedList.isEmpty()) {
            updatedList.remove(updatedList.size() - 1);
        }
        return updatedList;
    }

    // Yêu cầu 7
    public void printArrayListUsingForEach(ArrayList<String> list) {
        System.out.println("Dùng forEach:");
        for (String item : list) {
            System.out.println(item);
        }
    }

    // Yêu cầu 8
    public void printArrayListUsingIterator(ArrayList<String> list) {
        System.out.println("Dùng iterator:");
        Iterator<String> iterator = list.iterator();
        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }
    }

    // Yêu cầu 9
    public void printArrayListUsingForLoop(ArrayList<String> list) {
        System.out.println("Dùng vòng lặp for:");
        for (int i = 0; i < list.size(); i++) {
            System.out.println(list.get(i));
        }
    }

    // Yêu cầu 10
    public ArrayList<String> addColorToStart(ArrayList<String> list, String color) {
        ArrayList<String> updatedList = new ArrayList<>(list);
        updatedList.add(0, color);
        return updatedList;
    }

    // Yêu cầu 11
    public ArrayList<String> updateColorAtIndex(ArrayList<String> list, int index, String newColor) {
        ArrayList<String> updatedList = new ArrayList<>(list);
        if (index >= 0 && index < updatedList.size()) {
            updatedList.set(index, newColor);
        }
        return updatedList;
    }

    // Yêu cầu 12
    public ArrayList<String> createNameArrayList() {
        ArrayList<String> nameList = new ArrayList<>();
        nameList.add("John");
        nameList.add("Alice");
        nameList.add("Bob");
        nameList.add("Steve");
        nameList.add("John");
        nameList.add("Steve");
        nameList.add("Maria");
        return nameList;
    }

    public void printFirstIndexOfNames(ArrayList<String> list, String name1, String name2) {
        int indexOfName1 = list.indexOf(name1);
        int indexOfName2 = list.indexOf(name2);
        System.out.println("Vị trí đầu tiên của " + name1 + ": " + indexOfName1);
        System.out.println("Vị trí đầu tiên của " + name2 + ": " + indexOfName2);
    }

    // Yêu cầu 13
    public void printLastIndexOfNames(ArrayList<String> list, String name1, String name2) {
        int lastIndexOfName1 = list.lastIndexOf(name1);
        int lastIndexOfName2 = list.lastIndexOf(name2);
        System.out.println("Vị trí cuối cùng của " + name1 + ": " + lastIndexOfName1);
        System.out.println("Vị trí cuối cùng của " + name2 + ": " + lastIndexOfName2);
    }

    public static void main(String[] args) {
        Task52B20 task = new Task52B20();
        // 1
        System.out.println(task.task1());
        // Yêu cầu 2
        ArrayList<Integer> list1 = new ArrayList<>();
        ArrayList<Integer> list2 = new ArrayList<>();
        list1.add(1);
        list1.add(2);
        list1.add(3);
        list2.add(4);
        list2.add(5);
        list2.add(6);
        ArrayList<Integer> sumList = task.sumArrayLists(list1, list2);
        System.out.println("ArrayList được cộng thêm: " + sumList);

        // Yêu cầu 3
        ArrayList<String> colorList = task.createColorArrayList();
        System.out.println("Số lượng phần tử của ArrayList: " + colorList.size());

        // Yêu cầu 4
        System.out.println("Phần tử thứ 4 của ArrayList: " + task.getElementAtIndex(colorList, 3));

        // Yêu cầu 5
        System.out.println("Phần tử cuối cùng của ArrayList: " + task.getLastElement(colorList));

        // Yêu cầu 6
        ArrayList<String> updatedColorList = task.removeLastElement(colorList);
        System.out.println("ArrayList sau khi xóa phần tử cuối cùng: " + updatedColorList);

        // Yêu cầu 7
        task.printArrayListUsingForEach(colorList);

        // Yêu cầu 8
        task.printArrayListUsingIterator(colorList);

        // Yêu cầu 9
        task.printArrayListUsingForLoop(colorList);

        // Yêu cầu 10
        ArrayList<String> colorListWithAddedColor = task.addColorToStart(colorList, "Purple");
        System.out.println("ArrayList sau khi thêm màu vào đầu: " + colorListWithAddedColor);

        // Yêu cầu 11
        ArrayList<String> updatedColorList2 = task.updateColorAtIndex(colorList, 2, "Yellow");
        System.out.println("ArrayList sau khi sửa màu phần tử thứ 3: " + updatedColorList2);

        // Yêu cầu 12
        ArrayList<String> nameList = task.createNameArrayList();
        task.printFirstIndexOfNames(nameList, "Alice", "Mark");

        // Yêu cầu 13
        task.printLastIndexOfNames(nameList, "Steve", "John");
    }
}
