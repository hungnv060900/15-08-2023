import java.util.regex.Pattern;

public class Task52B30 {
    // Yêu cầu 1
    public String removeDuplicateCharacters(String str) {
        StringBuilder result = new StringBuilder();
        for (char ch : str.toCharArray()) {
            if (result.indexOf(String.valueOf(ch)) == -1) {
                result.append(ch);
            }
        }
        return result.toString();
    }

    // Yêu cầu 2
    public int countWords(String str) {
        String[] words = str.split("\\s+");
        return words.length;
    }

    // Yêu cầu 3
    public String concatenateStrings(String s1, String s2) {
        return s1.concat(s2);
    }

    // Yêu cầu 4
    public boolean containsSubstring(String s1, String s2) {
        return s1.contains(s2);
    }

    // Yêu cầu 5
    public char getCharacterAtIndex(String str, int index) {
        return str.charAt(index);
    }

    // Yêu cầu 6
    public int countCharacterOccurrences(String str, char ch) {
        int count = 0;
        for (char c : str.toCharArray()) {
            if (c == ch) {
                count++;
            }
        }
        return count;
    }

    // Yêu cầu 7
    public int indexOfFirstOccurrence(String str, char ch) {
        return str.indexOf(ch);
    }

    // Yêu cầu 8
    public String convertToUpperCase(String str) {
        return str.toUpperCase();
    }

    // Yêu cầu 9
    public String toggleCase(String str) {
        char[] chars = str.toCharArray();
        for (int i = 0; i < chars.length; i++) {
            if (Character.isLowerCase(chars[i])) {
                chars[i] = Character.toUpperCase(chars[i]);
            } else if (Character.isUpperCase(chars[i])) {
                chars[i] = Character.toLowerCase(chars[i]);
            }
        }
        return new String(chars);
    }

    // Yêu cầu 10
    public int countUpperCaseCharacters(String str) {
        int count = 0;
        for (char ch : str.toCharArray()) {
            if (Character.isUpperCase(ch)) {
                count++;
            }
        }
        return count;
    }

    // Yêu cầu 11
    public String generateUppercaseAlphabet() {
        StringBuilder result = new StringBuilder();
        for (char ch = 'A'; ch <= 'Z'; ch++) {
            result.append(ch);
        }
        return result.toString();
    }

    // Yêu cầu 12
    public void divideStringIntoEqualParts(String str, int n) {
        int length = str.length();
        if (length % n != 0) {
            System.out.println("KO");
            return;
        }

        int partLength = length / n;
        for (int i = 0; i < length; i += partLength) {
            System.out.println(str.substring(i, i + partLength));
        }
    }

    // Yêu cầu 13
    public  String removeAdjacentDuplicates(String str) {
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < str.length(); i++) {
            if (i == 0 || str.charAt(i) != str.charAt(i - 1)) {
                result.append(str.charAt(i));
            }
        }
        return result.toString();
    }

    // Yêu cầu 14
    public  String mergeStrings(String s1, String s2) {
        int minLength = Math.min(s1.length(), s2.length());
        return s1.substring(s1.length() - minLength) + s2.substring(s2.length() - minLength);
    }

    // Yêu cầu 15
    public  String toggleCaseAndReverse(String str) {
        char[] chars = str.toCharArray();
        for (int i = 0; i < chars.length; i++) {
            if (Character.isLowerCase(chars[i])) {
                chars[i] = Character.toUpperCase(chars[i]);
            } else if (Character.isUpperCase(chars[i])) {
                chars[i] = Character.toLowerCase(chars[i]);
            }
        }
        return new StringBuilder(new String(chars)).reverse().toString();
    }

    // Yêu cầu 16
    public  boolean containsDigit(String str) {
        return str.matches(".*\\d.*");
    }

    // Yêu cầu 17
    public  boolean isValidString(String str) {
        String regex = "^[A-Z][A-Za-z0-9]{0,18}[0-9]$";
        return Pattern.matches(regex, str);
    }

    public static  void main(String[] args) {
        Task52B30 task = new Task52B30();

        // Gọi các hàm thực hiện yêu cầu tương ứng và in ra kết quả
        System.out.println(task.removeDuplicateCharacters("bananas"));
        System.out.println(task.concatenateStrings("Devcamp java", "Abc  abc bac"));
        System.out.println(task.countWords("abc def"));
        System.out.println(task.toggleCase("DevCamp"));
        System.out.println(task.countUpperCaseCharacters("DevCamp123"));
        System.out.println(task.generateUppercaseAlphabet());
        task.divideStringIntoEqualParts("abcdefghijklmnopqrstuvwxy", 5);
        System.out.println(task.removeAdjacentDuplicates("aabaarbarccrabmq"));
        System.out.println(task.mergeStrings("Welcome", "home"));
        System.out.println(task.toggleCaseAndReverse("DevCamp"));
        System.out.println(task.containsDigit("DevCamp123"));
        System.out.println(task.isValidString("DEVCAMP123"));
    }
}
